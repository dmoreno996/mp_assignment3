﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace hierarchicalNavigationApp
{
    public partial class leagueCups : ContentPage
    {
        public leagueCups( string titles )
        {
            InitializeComponent();
            leagueTitles.Text = titles;
        }
    }
}
