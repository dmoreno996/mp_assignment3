﻿using Xamarin.Forms;
using System;
using System.Diagnostics;

namespace hierarchicalNavigationApp
{
    public partial class hierarchicalNavigationAppPage : ContentPage
    {
        public hierarchicalNavigationAppPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void Handle_Disappearing(object sender, System.EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void Handle_Appearing(object sender, System.EventArgs e)
        {
            //throw new NotImplementedException();
        }

        async void Handle_ClickedL(object sender, System.EventArgs e)
        {
            bool usersResponse = await DisplayAlert("WARNING",
                         "Do you really want to be blown away by this great history?",
                         "Blow me away Doctor",
                         "Not Today");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new leagueTitles("A massive 20 English League Titles!"));
            }

        }

        async void Handle_ClickedCL(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new championsLeague("4 incredible Champions League Titles!"));

        }

        async void Handle_ClickedLC(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new leagueCups("5 Massive League Cups"));
        }
    }
}
